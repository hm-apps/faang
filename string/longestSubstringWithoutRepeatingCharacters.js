/*
Given a string, find the length of the longest substring without repeating characters.

1. Contraints:
- is the substring continguous?
    - yes, look for a substring, not a subsequence
- case sensitivity?
    - no, assume all characters a lowercase
*/

// T: O(N)
// S: O(N)

const string = "abcb"

const lengthOfLongestSubstring = function(s) {
    if(s.length <= 1) return s.length; // edge case
    
    const seen = {}; 
    let left = 0; // 2
    let longest = 0;
    
    // right = 4
    for(let right = 0; right < s.length; right++) {
        const currentChar = s[right]; // s[3] = b
        const previouslySeenChar = seen[currentChar]; // = 1
   
        console.log('right:', right)
        console.log('currentChar:', currentChar)
        console.log('previouslySeenChar:', previouslySeenChar)

        if(previouslySeenChar >= left) { // 1 is > 0
          left = previouslySeenChar + 1; // so left = 1+1 =2
        }
        
        seen[currentChar] = right; // = {a:0, b:3, c: 2}
        
        longest = Math.max(longest, right - left + 1); // 3
    }
    
    return longest;
};

console.log(lengthOfLongestSubstring(string));



/*
// Brute force //
const string = "abccabb"

// Time: O(N^2);
// Space: O(N)

const lengthOfLongestSubstring = function(s) {
    if(s.length <= 1) return s.length;
    
    let longest = 0;
    
    for(let left = 0; left < s.length; left++) {
      let seenChars = {}, currentLength = 0;

      for(let right = left; right < s.length; right++) {
        const currentChar = s[right];

        if(!seenChars[currentChar]) {
          currentLength++;
          seenChars[currentChar] = true;  
          longest = Math.max(longest, currentLength);
        } else {
          break;
        }
      }
    }
    
    return longest;
};

console.log(lengthOfLongestSubstring(string));

module.exports = lengthOfLongestSubstring;
*/