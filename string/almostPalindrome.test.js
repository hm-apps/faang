const isAlmostPalindrome = require('./almostPalindrome');

describe(isAlmostPalindrome, () => {
    it.each([
        [ "", true ],
        [ "a", true ],
        [ "ab", true ],
        [ "race a car", true ], // remove either, still works
        [ "race car", true ], // is palindrome
        [ "abccdba", true ], // removing 1 will work, removing the other will not
        [ "abcdefdba", false ],


    ])("string %s", (string, answer) => {
        
        const result = isAlmostPalindrome(string, answer)

        console.log("RESULT:", result)
        expect(result).toEqual(answer)
    })
})