__notes started with Question #5__  
longestSubstringWithoutRepeatingCharacters

# Question 5 
longestSubstringWithoutRepeatingCharacters

## Constraint Questions
- If ask for a substring: 
    - Is the string _contiguous_?
        - sequential and no breaks inbetween
        - **Substring vs Subsequence**
            - Substring = contiguous
                - "abcbbd" => "abc"
            - Subsequence = not contiguous (can have breaks)
                - "abcbbd" => "abcd"
- Does case sensitivity matter?

## Sliding Window Technique
- similar to the 2 pointer technique, but there are more requirements
- Form a window over some **portion of sequential data**, then move that window throughout the data to capture different parts of it.
    - sequential = order of data is important
    - = applied to strings, arrays, and sometimes linked lists
    - size can be whatever and can change throughout
    - window can jump around


## HINTS for optimal solution
- use a sliding window to represent the current substring evaluating
- size of window will change, based on characters, & characters we've already seen should shrink size
- our seen characters hashmap keeps track of what characters we have seen, and the index we saw them at

# Question 6 - Palindromes 

- A string that reads the same forwards and backward
    - single alphanum = true
    - empty string = true
    - spaces, capitals, symbols - disregard
        - usually do a transformation to remove all that before start working

- Palindromes are subproblems of strings
    - **subproblem** problem we have to solve along the way to solve the main problem  

- 1 strategy
    - Left pointer on index 0
    - Right pointer index lenght - 1 
    - compare to see if match
    - move towards the middle
    - **odd = converge on same character**
    - **even = same character twice in middle**

- 2 strategy
    - both pointers start in the middle
    - odd length = same middle character
    - even length = 2 middle characters

- 3 strategy
    - create duplicate but backwards
    - pointers on both left 

> Given a string, determine if is a palindrome, considering only alphanumeric characters, ignoring case sensitivity.  

> Given a string, determine if is ALMOST a palindrome. If it becomes a palindrome by removing 1 character. Alphanumeric & not case sensitive

