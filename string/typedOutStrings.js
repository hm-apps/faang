const string1 = "#ab#z"
const string2 = "az#z"

var backspaceCompare = function(S, T) {
    let p1 = S.length - 1, p2 = T.length - 1; // start from right side/end
    
    while(p1 >= 0 || p2 >= 0) { // since strings could be different lengths, end when 1 hits index 0
        if(S[p1] === "#" || T[p2] === "#") { // if either = #, calculate how many spaces to move
            if(S[p1] === "#") { // arbitrarily chose S; check for # again 
                let backCount = 2; // standard logic for backspace
                
                while(backCount > 0) { // need to keep track of how many backspaces there will need to be
                                        // when there is only 1 = there was only 1 hash
                    p1--; // (move pointer)
                    backCount--; // by moving p1 back 1, we have consumed 1 of the back counts already
                    
                    if(S[p1] === "#") { // if the next character is also a #, 
                        backCount += 2; // jump back 2 again
                    } // if not, loop again
                }
            }
            
            if(T[p2] === "#") { // same logic for other string
                let backCount = 2;
                
                while(backCount > 0) {
                    p2--;
                    backCount--;
                    
                    if(T[p2] === "#") {
                        backCount += 2;
                    }
                }
            }
        } else { // if neither are #
            if(S[p1] !== T[p2]) { // check if they are not equal
                return false; // false
            } else { // are equal
                p1--; // move down the strings
                p2--;
            }
        }
    }
    
    return true;
};

console.log(backspaceCompare(string1, string2));


// const string1 = "abc#d"
// const string2 = "az#z"

// const buildStringBrute = function(string) {
//     const builtString = [];
//     for(let p = 0; p < string.length; p++) { // O(n)
//         if(string[p] !== '#') {
//             builtString.push(string[p]);
//         } else {
//             builtString.pop();
//         }
//     }
    
//     return builtString;
// }

// var backspaceCompare = function(S, T) {
//     const finalS = buildString(S); // O(a)
//     const finalT = buildString(T); // O(b) // = O(a+b)
    
//     if(finalS.length !== finalT.length) {
//         return false
//     } else {
//         for(let p = 0; p < finalS.length; p++) { // O(a) or O(b) // = O(2a+b) or O(a+2b) // = O(a+b)
//             if(finalS[p] !== finalT[p]) {
//                 return false
//             }
//         }
//     }
    
//     return true;
// };

// console.log(backspaceCompare(string1, string2));

// Time = O(a+b)
// Space = O(a+b)

/////////////////////////////////////////////////

// return true or false
// start from right (end)
    // since nothing to right, no hash = will be in final string
// 2 pointer structure
    // 1 pointing to S element
    // 1 pointing to T element
        // comparing the letters

// if p1 and p2 match, we can move pointer
    // else return false

// when encounter hash = move pointer over by 2

