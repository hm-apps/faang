const longestSubstringWithoutRepeatingCharacters = require('./longestSubstringWithoutRepeatingCharacters');

describe(longestSubstringWithoutRepeatingCharacters, () => {
    it.each([
        [ "abccabb", 3 ],
        [ "abc", 3 ],
        [ "abbcd", 3 ],
        [ "", 0 ],
        [ "a", 1 ],
        [ "aaaaa", 1 ],
        [ "abbb", 2 ],
        [ "abababbac", 3 ],
        [ "abcbda", 4 ], // overlapping: "abc", "cbda"
    ])("string %s", (string, answer) => {
        const result = longestSubstringWithoutRepeatingCharacters(string, answer)
        console.log("RESULT:", result)
        expect(result).toEqual(answer)
    })
})