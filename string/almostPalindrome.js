// a palindrome is also an almost palindrome

var isAlmostPalindrome = function(s) {
    let start = 0;
    let end = s.length - 1;
    while (start < end) {
        if (s[start] !== s[end]) {
            return validSubPalindrome(s, start + 1, end) || validSubPalindrome(s, start, end - 1);
        }
        start++;
        end--;
    }
    return true;
  };
  
  var validSubPalindrome = function(s, start, end) {
    while (start < end) {
        if (s[start] !== s[end]) {
            return false;
        }
        start++;
        end--;
    }
    return true;
  };


// const isAlmostPalindrome = (s) => {
//     s = s.replace(/[^A-Za-z0-9]/g, "").toLowerCase();
//     let left = 0, right = s.length - 1
//     while(left < right) {
//         if (s[left] !== s[right]) {
//             let rev = "";
//             substring = s.replace(s[left], '');
//             for(let i = substring.length - 1; i >= 0; i--) {
//                 rev += substring[i];
//             }
        
//             if(rev === substring) {
//                 return true;
//             }

//             let rev2 = "";
//             substring2 = s.replace(s[right], '');

//             for(let i = substring2.length - 1; i >= 0; i--) {
//                 rev2 += substring2[i];
//             }

//             if(rev === substring2) {
//                 return true;
//             }

//             return false;
//         }

//         left++
//         right--
//     }
//     return true;
// }

module.exports = isAlmostPalindrome;