const isValidPalindrome = require('./palindrome');

describe(isValidPalindrome, () => {
    it.each([
        [ "aabaa", true ],
        [ "aabbaa", true ],
        [ "a", true ],
        [ "", true ],
        [ "A man, a plan, a canal: Panama", true ],
        [ "abc", false ],

    ])("string %s", (string, answer) => {
        const result = palindrome(string, answer)
        console.log("RESULT:", result)
        expect(result).toEqual(answer)
    })
})