/*
NOTE: The beginning portion builds our test case linked list. Scroll down to the section titled Our Solution for the code solution!
 */

class ListNode {
    constructor(val, next = null, prev = null, child = null) {
      this.val = val;
      this.next = next;
      this.prev = prev;
      this.child = child;
    }
  }
  
  // ---- Generate our linked list ----
  //const nodes = [1, [2, 7, [8, 10, 11], 9], 3, 4, [5, 12, 13], 6]
  const nodes = [1, 2, [7,8,9], 3]
  
  const buildMultiLevel = function(nodes) {
    const head = new ListNode(nodes[0]);
    let currentNode = head;
  
    for(let i = 1; i < nodes.length; i++) {
      const val = nodes[i];
      let nextNode;
  
      if(Array.isArray(val)) {
        nextNode = buildMultiLevel(val);
        currentNode.child = nextNode;
        continue;
      }
  
      nextNode = new ListNode(val);
      currentNode.next = nextNode;
      nextNode.prev = currentNode;
      currentNode = nextNode;
    }
    
    return head;
  }
  
  let multiLinkedList = buildMultiLevel(nodes);
  
  // ---- Generate our linked list ----
  
  const printListMulti = head => {
    if(!head) {
      return;
    }
  
    console.log(head.val)
  
    if(head.child) {
      printListMulti(head.child);
    }
  
    printListMulti(head.next);
  }
  
  const printList = head => {
    if(!head) {
      return;
    }
  
    console.log(head.val);
    
    printList(head.next);
  }

console.log(multiLinkedList)

/// Solution

const flattenMultilevel = (head) => {
    if(!head) return head; // if head is null
    let currentNode = head;

    while(currentNode !== null){ // advancing until reach the end (all linked list iterations)
        if(currentNode.child === null){ // only need to do something when there is a child
            currentNode = currentNode.next // advance currentNode 
        } else { // perform the merge if there is a child
            let tail = currentNode.child; // initializing to find the tail of this child node
            while(tail.next !== null){ // iterate through until you find and re-assign the tail (don't care about inbetween values)
                tail = tail.next
            }
            tail.next = currentNode.next; // merge the tail into the list 
            if(tail.next !== null){
                tail.next.prev = tail; // the previous of the current's next, needs be now be the tail
            }
            // Re-pointing the rest of the important points
            currentNode.next = currentNode.child;
            currentNode.next.prev = currentNode;
            currentNode.child = null;
        }
    }
    console.log(head)
    return head;
}

flattenMultilevel(multiLinkedList)

//space = O(1)
//time = O(2n) = O(n) - touched twice (currentNode and tail pointer)