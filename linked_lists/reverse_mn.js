/*
NOTE: The beginning portion builds our test case linked list. Scroll down to the section titled Our Solution for the code solution!
 */

class ListNode {
    constructor(val, next = null) {
      this.val = val;
      this.next = next;
    }
  }
  // ---- Generate our linked list ----
  const linkedList = [6, 5, 4, 3, 2, 1].reduce((acc, val) => new ListNode(val, acc), null);
  
  // ---- Generate our linked list ----
  
  const printList = (head) => {
    if(!head) {
      return;
    }
  
    console.log(head.val);
    printList(head.next);
  }
  
  // --------- Our solution -----------
  
  var reverseBetween = function(head, m, n) {
    let pointer = 1;                    // currentPosition: keeping track of where located in the linked list
    let currentNode = head;             // node we want to work with
    let start = head;                   // value of list node at m-1 // start of reverse section // init as head for someplace to start
    
    // with this we will know the m-1 and m = currentNode & currentPosition/pointer
    while(pointer < m) {                // when = m, we are at the start of reversal section
      start = currentNode;              // start = m-1 // value before reversal begins
      currentNode = currentNode.next;   // advance currentNode forward by 1
      pointer++;                        // increment current position
    }
    
    let newList = null;                 // the list so far
    let tail = currentNode;             // m; the first value in the start of the reversal
    
    // iterate over reversal steps
    while(pointer >= m && pointer <= n) {   // within boundaries of m and n
      const next = currentNode.next;        // saving next value
      currentNode.next = newList;           // 
      newList = currentNode;                // currentNode = the head of the new list
      currentNode = next;                   // = next value cached above
      pointer++;                            // pointer increment
    }
    
    start.next = newList;
    tail.next = currentNode;
    
    if(m > 1) {
      return head
    } else {
      return newList;
    } 
  };
  
  printList(linkedList);
  console.log('after reverse');
  printList(reverseBetween(linkedList, 2, 5));
  