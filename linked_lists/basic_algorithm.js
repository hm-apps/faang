/*
NOTE: The beginning portion builds our test case linked list. Scroll down to the section titled Our Solution for the code solution!
 */

class ListNode {
    constructor(val, next = null) {
      this.val = val;
      this.next = next;
    }
  }
  // ---- Generate our linked list ----
  const linkedList = [5, 4, 3, 2, 1].reduce((acc, val) => new ListNode(val, acc), null);
  
  // ---- Generate our linked list ----
  
  const printList = (head) => {
    if(!head) {
      return;
    }
  
    console.log(head.val);
    printList(head.next);
  }
  
  // --------- Our solution -----------
  
  var reverseList = function(head) {
    // Init pointers
    let prev = null;
    let current = head;
    
    while(current) {
      let nextTemp = current.next; // save next
      current.next = prev; // reverse
      
      // advance prev and curr
      prev = current; 
      current = nextTemp;
    }
    
    return prev; // new head at end
  };
  
  printList(linkedList);
  console.log('after reverse')
  printList(reverseList(linkedList))

  // Time: O(n)
  // Space: O(1); never creating a new linked list

/*
  Steps
  1. get current node
  2. store next value
  3. update next value to list so far
  4. store current node as list so far
  5. update node to stored next value at (2)
*/