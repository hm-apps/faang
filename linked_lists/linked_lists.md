# Linked Lists

- 2D data model
- low level data structure (fundamental) = used in other data structures
- singly vs doubly
- contends a set of nodes
    - nodes have 2 parts: value and the pointer to the next node in line
    - 1st node = head
    - last node = tail
- null terminated = end of list
    - tail's pointer points to null
- JavaScript does not come with linked lists built in
- can insert in the middle because can just re-point
    - traversal is the same as iteration in array; but slower
- array items are stored next to each other while linked lists can be stored all over memory (like hash tables)  

- **Pointer**
    - a reference to another place in memory
        ```js
        let obj1 = {a:true}
        let obj2 = obj1 // this is a pointer
        obj1.a = 'no'
        console.log('1', obj1) // {a:no}
        console.log('2', obj2) // {a:no}
        delete obj1
        console.log('1', obj1) // no defined
        console.log('2', obj2) // {a:no}

        // JavaScript garbage collection (manages memory automatically)
        obj2 = 'hello'
        obj1 // undefined
        obj2 // 'hello'
        // only when reassigned does {a:true} value get removed from memory
        ```

- **Doubly Linked List**
    - links to the node before it
    - allows us to traverse list backwards
    - searching can me more efficient
    - but holds more memory

| Singly      | Doubly |
| ----------- | ----------- |
| simple implementation      | complex implementation       |
| less memory   | more memory and storage        |
| faster      |        |
| only iterate forwards   | iterate backwards and forwards        |
| can lose completely if lose head node      |        |
| use when memory is expensive   | use when not limited on memory        |
| use when need fast insert and delete      | more operations with insert and delete       |
| not much searching   | good for searching        |
| interview problems | interview concept |

| Pros      | Cons |
| ----------- | ----------- |
| Ordered      | Slow lookup       |
| Fast Insertion and deletion   | more memory        |
| simplicity |  |
| ability to grow and shrink as needed |  |
| flexible size |  |

- there is no random access (when looking for something you have to traverse, not just know the index)
- very fast if insert or delete is at the beginning or end

- **Real examples**
    - file systems on computer
    - browser history

- **Big O**
    - prepend O(1)
    - append O(1)
    - lookup O(n)
    - insert O(n)
    - delete O(n)

> Will always either receive the head or return the head  
> There isn't always a tail (may loop back around and point to an already seen node) = cycle

## Basic Algorithm
"Given a linked list, return it in reverse."

- Steps
    1. get current node
    1. store next value
    1. update next value to list so far
    1. store current node as list so far
    1. update node to stored next value at (2)


1. Constraints
    - return if get null or single node? = null & node back
1. Test Cases
    - 1 -> 2 -> 3 -> 4 -> null
        - ==> 4 -> 3 -> 2 -> 1 -> null
    - 2
        - ==> 2 -> null
    - null 
        - ==> null

> We won't ever know the length at the start of the function   

- **Standard Pattern**
    ```js
    function(head){
        let currentNode = head;
        while (currentNode) {
            // perform operation
            currentNode = currentNode.next;
        }
    ```


"Given a linked list and numbers m and n, return it back with only positions m to n in reverse."
1. constraints
- will m and n always be within the bounds of the linked list? = yes
- can we receive m and n values for the whole linked list? = yes, can be m=1 and n=length
1. Test cases
- 1 -> 2 -> 3 -> 4 -> 5 -> null ; m = 2, n = 4
    - ==> 1 -> 4 -> 3 -> 2 -> 5 -> null
- 1 -> 2 -> 3 -> 4 -> 5 -> null ; m = 1, n = 5
    - ==> 5 -> 4 -> 3 -> 2 -> 1 -> null
- 5; m = 5, n=5
- null ; m = 0, n = 0


 