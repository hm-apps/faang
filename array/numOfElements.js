function numOfElements(n) {
    return new Array(n).fill(0).map((_, idx) => idx)
}

module.exports = numOfElements
