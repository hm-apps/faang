// // const elevationArray = [0, 1, 0, 2, 1, 0, 3, 1, 0, 1, 2]
// const elevationArray = [1,4,2,3]

// const getTrappedRainwater = function(heights) {
//   let totalWater = 0;
  
//   for(let p = 0; p < heights.length; p++) {
//     let leftP = p, rightP = p, maxLeft = 0, maxRight = 0;


//     while(leftP >= 0) {
//       console.log('while(leftP >= 0)')
//       maxLeft = Math.max(maxLeft, heights[leftP]);
//       leftP--;
//     }

//     while(rightP < heights.length) {
//       maxRight = Math.max(maxRight, heights[rightP]);
//       rightP++;
//     }
    
//     const currentWater = Math.min(maxLeft, maxRight) - heights[p];
    
//     if(currentWater >= 0) {
//       totalWater += currentWater;
//     }
//   }
  
//   return totalWater;
// }

// console.log(getTrappedRainwater(elevationArray));

const elevationArray = [0, 1, 0, 2, 1, 0, 3, 1, 0, 1, 2]

/*
1. Identify the pointer with the lesser value
2. Is this pointer value greater than or equal to max on that side
  yes -> update max on that side
  no -> get water for pointer, add to total
3. move pointer inwards
4. repeat for other pointer
 */

const getTrappedRainwater = function(heights) {

  let left = 0, right = heights.length - 1, totalWater = 0, maxLeft = 0, maxRight = 0;
  
  while(left < right) { // iterate over the pointers until they touch each other 
    
    /* left side logic (because left side is small (smaller determines the height to use)) */
    if(heights[left] <= heights[right]) { // identify the pointer with the lesser value

      if(heights[left] >= maxLeft) { 
        maxLeft = heights[left] // if the left height is greater than or equal the max left, then update the max
      } else { 
        totalWater += maxLeft - heights[left]; // else, add water at pointer value
      }
      left++;
      
    /* right side logic */
    } else {

      if(heights[right] >= maxRight) {
          maxRight = heights[right];
      } else {
          totalWater += maxRight - heights[right];
      }
        
      right--;
    }
  }

  return totalWater;
}


console.log(getTrappedRainwater(elevationArray));