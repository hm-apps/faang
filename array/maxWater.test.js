const getMaxWaterContainer = require ('./maxWater.js');

describe(getMaxWaterContainer, () => {
    it.each([
       [ [ 7,1,2,9 ], 21 ],
       [ [3,6,10,1], 6 ],
       [[11, 5, 8, 1, 17, 1], 44]
    ])("from array %s", (array, answer) => {
        const result = getMaxWaterContainer(array)
        console.log('RESULT:', result)
        expect(result).toEqual(answer)
    })

})