const twoSumBrute = (array, target) => {
    for(let p1 = 0; p1 < array.length; p1++) {
        let numToFind = target - array[p1]
        for(let p2= p1 + 1; p2<array.length; p2++){
            if(numToFind === array[p2]){
                return [p1, p2]
            }
        }
    }
    return null

}

const twoSum = (arr, target) => {
    const numsMap = {}
    for (let x = 0; x < arr.length; x++){
        const curMapVal = numsMap[arr[x]]
        if (curMapVal >= 0) {
            return [curMapVal, x]
        } else {
            const numToFind = target - arr[x]
            numsMap[numToFind] = x
        }
    }
    return null
}


module.exports = twoSum

