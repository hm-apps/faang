const twoSum = require ('./twoSum.js');

describe(twoSum, () => {
    it.each([
        [ [ 1,2,3,5 ], 6, [0,3]],
        [ [ 14,222,33,10,55 ], 24, [0,3]],
        [ [ 1,2,3,5,44,10 ], 12, [1,5]],
        [ [ 1, 3, 6, 99], 9, [1,2]],
        [ [ 5,6,7 ], 28, null],
        [ [], 111, null],
        [ [ 5 ], 777777, null]
    ])("from array %s, with target %s", (array, target, indices) => {
        const result = twoSum(array, target)
        console.log('RESULT', result)
        expect(result).toEqual(indices)
    })

})