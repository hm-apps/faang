const numOfElements = require('./numOfElements')

describe('numOfElements', () => {
    it.each([
        [1, [0]],
        [5, [0,1,2,3,4]]
    ])('numOfElements with %s', (num, expected) => {
        const result = numOfElements(num)

        expect(result).toEqual(expected)
    })
})
